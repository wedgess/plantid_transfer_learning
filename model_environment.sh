#!/bin/bash

# text colours
RED=$(tput setaf 1)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
CYAN=$(tput setaf 6)
ULINE=$(tput smul)    # Enable underline mode
RULINE=$(tput rmul)    # Disable underline mode
BOLD=$(tput bold)
RESET=$(tput sgr0)

# Default values
IMAGE_SIZE=224 # The size of the images used to retrain final layer
RETRAIN_STEPS=400 # itterations for retraining
TRAIN_ONLY=false
CONVERT_TFLITE=true
AUGMENT=false
LEARNING_RATE=0.01

CURRENT_WORKING_DIR=$(pwd)

usage() {
  echo -e "\n${BOLD}If you are running this script for the first time you must run it without the ${ULINE}\"--train-only\"${RULINE} and ${ULINE}\"--convert-tflite\"${RULINE} arguments set.${RESET}"
  echo -e "\nIf you wish to change the number of retrain steps you can set the ${BOLD}\"--steps\"${RESET} argument."
  echo -e "If you wish to change the input image size you can set the ${BOLD}\"--image-size\"${RESET} argument."
  echo ""
  echo -e "${ULINE}The scripts default arguments can be seen below.${RULINE}\n"
  echo "${BLUE}./model_environment.sh"
  echo -e "\t${CYAN}-h --help"
  echo -e "\t-a or --augment=${AUGMENT}"
  echo -e "\t-l or --learning_rate=${LEARNING_RATE}"
  echo -e "\t-i or --image-size=${IMAGE_SIZE}"
  echo -e "\t-s or --steps=${RETRAIN_STEPS}"
  echo -e "\t-t or --train-only=${TRAIN_ONLY}"
  echo -e "\t-c or --convert-tflite=${CONVERT_TFLITE}${NORMAL}"
  echo ""
}

print_params() {
  echo ""
  echo -e "${ULINE}${BLUE}Script parameters:${RULINE}\n${YELLOW}"
  echo -e "\t DATA_AUGMENTATION=${AUGMENT}"
  echo -e "\t LEARNING_RATE=${LEARNING_RATE}"
  echo -e "\t IMAGE_SIZE=${IMAGE_SIZE}"
  echo -e "\t STEPS=${RETRAIN_STEPS}"
  echo -e "\t TRAINING_ONLY=${TRAIN_ONLY}"
  echo -e "\t CONVERT TF_FILES=${CONVERT_TFLITE}${NORMAL}"
  echo ""
}

convert_from_seconds() {
  ((h=${1}/3600))
  ((m=(${1}%3600)/60))
  ((s=${1}%60))
  if [ $h -eq 0 ]; then
    if [ $m -eq 0 ]; then
      printf "%02d Seconds\n" $s
    else
      printf "%02d Minutes %02d Seconds\n" $m $s
    fi
  else
    printf "%02d Hours %02d Minutes %02d Seconds\n" $h $m $s
  fi
}

download_data_set() {
  # script to download shareable Google Drive link - Credits: https://stackoverflow.com/a/38937732/3748532
  ggID='1DqQfQn02IVrtbJIH0rCAjWoTRki_dAiI'
  ggURL='https://drive.google.com/uc?export=download'
  FILENAME="$(curl -sc /tmp/gcokie "${ggURL}&id=${ggID}" | grep -o '="uc-name.*</span>' | sed 's/.*">//;s/<.a> .*//')"
  CODE="$(awk '/_warning_/ {print $NF}' /tmp/gcokie)"
  curl -Lb /tmp/gcokie "${ggURL}&confirm=${CODE}&id=${ggID}" -o "${FILENAME}"
  echo -e "${YELLOW}Extracting model data..${RESET}"
  tar xzf ${FILENAME}
  rm -f ${FILENAME}
  FOLDERNAME="$(echo "${FILENAME}" | sed 's/\..*$//')"
  mv ${FOLDERNAME} ./tf_files/${FOLDERNAME}
}

download_env() {
  echo -e "\n${YELLOW}Downloading Tensorflow retraining scripts...${RESET}"
  # clone the tensorflow for poets repo
  git clone "https://github.com/googlecodelabs/tensorflow-for-poets-2" tensorflow_build_env
  cp -r "${CURRENT_WORKING_DIR}/tensorflow_build_env/scripts/" "${CURRENT_WORKING_DIR}/"
  rm -rf "${CURRENT_WORKING_DIR}/tensorflow_build_env"
}

download_model_data() {
  # make model directory to extract model to
  mkdir -p "${CURRENT_WORKING_DIR}/tf_files/models"
  echo -e "\n${YELLOW}Downloading and extracting pretrained model...${RESET}"
  # download the model
  curl -s "http://download.tensorflow.org/models/mobilenet_v1_2018_02_22/mobilenet_v1_1.0_224.tgz" | tar xz -C "${CURRENT_WORKING_DIR}/tf_files/models"

  echo -e "${YELLOW}Downloading flowers dataset...${RESET}"
  download_data_set
}

retrain() {
  echo -e "\n${YELLOW}Retraining models final output layer...${RESET}"
  # set up variables required by TF
  ARCHITECTURE="mobilenet_0.50_${IMAGE_SIZE}"

  # retrain last layer on new images
  COMMAND="python3.6 -m scripts.retrain \
    --bottleneck_dir="${CURRENT_WORKING_DIR}/tf_files/bottlenecks" \
    --how_many_training_steps=${RETRAIN_STEPS} \
    --model_dir="${CURRENT_WORKING_DIR}/tf_files/models/" \
    --summaries_dir="${CURRENT_WORKING_DIR}/tf_files/training_summaries/${ARCHITECTURE}" \
    --output_graph="${CURRENT_WORKING_DIR}/tf_files/retrained_graph.pb" \
    --output_labels="${CURRENT_WORKING_DIR}/tf_files/retrained_labels.txt" \
    --architecture="${ARCHITECTURE}" \
    --image_dir="${CURRENT_WORKING_DIR}/tf_files/flower_photos" \
    --print_misclassified_test_images True \
    --learning_rate=${LEARNING_RATE}"

    if [ "${AUGMENT}" = true ]; then
        COMMAND="$COMMAND --flip_left_right True \
        --random_crop 10 \
        --random_scale 10 \
        --random_brightness 10"
    fi

    $($COMMAND)
}

build_toco() {
  echo -e "\n${YELLOW}Downloading Tensorflow source...${RESET}"
  # build toco to convert .pb file to .lite file
  git clone "https://github.com/tensorflow/tensorflow" tf-source
  echo -e "\n${YELLOW}Building toco binary to convert to lite format...${RESET}"
  cd "${CURRENT_WORKING_DIR}/tf-source"
  bazel build tensorflow/contrib/lite/toco:toco
  cd "${CURRENT_WORKING_DIR}"
}

convert_tflite() {
  echo -e "\n${YELLOW}Converting to model to lite format...${RESET}"
  cd "${CURRENT_WORKING_DIR}/tf-source"
  # use bazel to create .lite file
  bazel-bin/tensorflow/contrib/lite/toco/toco \
    --input_file="${CURRENT_WORKING_DIR}/tf_files/retrained_graph.pb" \
    --output_file="${CURRENT_WORKING_DIR}/tf_files/retrained_graph.lite" \
    --input_format=TENSORFLOW_GRAPHDEF \
    --output_format=TFLITE \
    --input_shape=1,${IMAGE_SIZE},${IMAGE_SIZE},3 \
    --input_array=input \
    --output_array=final_result \
    --inference_type=FLOAT \
    --input_type=FLOAT
  cd "${CURRENT_WORKING_DIR}"
  echo -e "${YELLOW}Conversion to lite format complete.${RESET}"
  echo -e "${BLUE}Model can be found at:${RESET} \"${CURRENT_WORKING_DIR}/tf_files/retrained_graph.lite\""
  echo -e "${BLUE}Labels can be found at:${RESET} \"${CURRENT_WORKING_DIR}/tf_files/retrained_labels.txt\""
}

# parse script arguments
while [ "$1" != "" ]; do
  PARAM=`echo $1 | awk -F= '{print $1}'`
  VALUE=`echo $1 | sed 's/^[^=]*=//g'`
  case $PARAM in
    -h | --help)
      usage
      exit
      ;;
    -a | --augment)
      AUGMENT=$VALUE
      ;;
    -i | --img-size)
      IMAGE_SIZE=$VALUE
      ;;
    -l | --learning-rate)
      LEARNING_RATE=$VALUE
      ;;
    -s | --steps)
      RETRAIN_STEPS=$VALUE
      ;;
    -t | --train-only)
      TRAIN_ONLY=$VALUE
      ;;
    -c | --convert-tflite)
      CONVERT_TFLITE=$VALUE
      ;;
    *)
      echo -e "\n${RED}${BOLD}[ERROR] Unknown parameter ${ULINE}\"${PARAM}\"${RULINE}${RESET}"
      usage
      exit 1
      ;;
  esac
  shift
done

SECONDS=0 ;
# if training only just retrain final layer
print_params
if [ "${TRAIN_ONLY}" = true ]; then
  retrain
  if [ "${CONVERT_TFLITE}" = true ]; then
    convert_tflite
  fi
else
  # perform full environment setup
  download_env
  download_model_data
  build_toco
  retrain
  convert_tflite
fi

echo -e "${YELLOW}[INFO] Time taken: $(convert_from_seconds ${SECONDS})"
