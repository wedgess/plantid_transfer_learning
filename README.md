# About
This is a build environment for transfer learning using Tensorflow. This script retrains the MobileNet model's final layer on plants/flowers. It achieves roughly 94% accuracy on the current dataset with 4000 steps. This project is part of [PlantID Frontend](https://bitbucket.org/wedgess/plantid_frontend) which uses the model from this environment.

---------------

# Prerequisite
- You must have Python installed.
- You must have [Tensorflow installed](https://www.tensorflow.org/install/).
- You must have [Bazel installed](https://docs.bazel.build/versions/master/install.html).

---------------

## Setup
In order to setup the build environment the **model_environment** script must be run.

- If the environment is not yet setup you can do so by running the following command: ```source model_environment.sh```
- By default the image input size is **224** this can be changed by passing the argument **--img-size** when executing the script: ```source model_environment.sh --img-size=224```
- The number of steps for training can be changed by passing the argument **--steps** when executing the script: ```source model_environment.sh --steps=4000```
- To see all available arguments for the script execute the following: ```source model_environment.sh --help```

---------------

### Script arguments

- **-i** or **--image-size** - Is the size of the image to be fed into the model. By default this value is 224.
- **-s** or **--steps** - Is the number of steps to retrain the model.
- **-t** or **--train-only** - Trains the model only, false by default (this should be used on subsequent runs after the initial setup).
- **-c** or **--convert-tflite** - Converts the retrained model to .lite format so that the model can be run on Android or iOS using Tensorflow Lite library.
- **-a** or **--augment** - true or false: If true data augmentation is used for training
- **-l** or **--learning-rate** - set the learning rate 
